import React, { useState, useEffect } from "react"
import MSearch from "./component/molecules/MSearch"
import MNav from "./component/molecules/MNav"
import MFirstCard from "./component/molecules/MFirstCard"
import MCard from "./component/molecules/MCard"
import { CSSTransition, TransitionGroup } from "react-transition-group"
import { v1 as uuid } from "uuid"
import { BrowserView, MobileView } from 'react-device-detect';
import { Icon } from "@iconify/react"

const App = () => {
  //store the data
  const [data, setData] = useState([])
  const [condition, setCondition] = useState(true)
  const [isTemp, setTemp] = useState(false)

  const funcSetTemp = (temp) => {
    setTemp(!temp)
  }
  const funcSetData = (city) => {
    setData([...data, { id: uuid(), city }])
  }

  const funcUnSetData = (id) => {
    setData(data.filter((item) => item.id !== id))
  }

  useEffect(() => {
    if (data.length >= 2) {
      setCondition(false)
    } else {
      setCondition(true)
    }
  })

  return (
    <>
      <BrowserView>
        <MNav isTemp={isTemp} funcSetTemp={funcSetTemp} />
        <MSearch funcSetData={funcSetData} condition={condition} />
        <TransitionGroup className="todo-list">
          {data.length === 0 ? (
            <MFirstCard />
          ) : (
            <div className="container mx-auto flex items-center justify-center">
              {data.map((data, idx) => {
                return (
                  <CSSTransition key={data.id} timeout={500} classNames="item">
                    <MCard
                      key={idx}
                      data={data}
                      funcUnSetData={funcUnSetData}
                      isTemp={isTemp}
                    />
                  </CSSTransition>
                )
              })}
            </div>
          )}
        </TransitionGroup>
      </BrowserView>
      <MobileView>
        <div className="container mx-auto py-5 h-screen flex items-center justify-center">
          <div className="flex justify-between">
            <Icon icon='akar-icons:desktop-device' className="mr-1 text-gray-800 font-bold" style={{ fontSize: '24px' }} />
            <p className="text-center text-gray-800 font-bold">Desktop only</p>
          </div>
        </div>
      </MobileView>
    </>
  )
}

export default App
