import React, { useState } from "react"
import { Icon } from "@iconify/react"
import PlacesAutocomplete from "react-places-autocomplete"

const MSearch = ({ condition, funcSetData }) => {
  const [search, setSearch] = useState("")

  const handleSubmit = () => {
    funcSetData(search)
    setSearch("")
  }

  const handleSelect = async (value) => {
    // const myArray = value.split(", ")
    funcSetData(value)
    setSearch("")
  }

  return (
    <PlacesAutocomplete
      value={search}
      onChange={setSearch}
      onSelect={handleSelect}
    >
      {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
        <>
          <div className="container mx-auto py-5 flex flex-col items-center justify-center">
            <div
              htmlFor="search"
              className="border border-gray bg-white flex justify-between items-center w-5/6 mx-1 px-1 py-2 rounded "
            >
              <Icon
                icon="akar-icons:search"
                className="text-primary text-lg mx-2"
              />
              <input
                {...getInputProps({
                  placeholder: "Search Places ...",
                })}
                disabled={!condition ? "disabled" : ""}
                className="form-input w-full py-1 outline-none"
              />
            </div>
          </div>
          <div className="container mx-auto flex flex-col items-center justify-center absolute inset-x-0 top-21">
            {loading ? (
              <div>
                <p>...loading</p>
              </div>
            ) : null}
            {suggestions.map((suggestion, idx) => {
              return (
                <div
                  key={idx}
                  {...getSuggestionItemProps}
                  className="border border-gray bg-white flex justify-between items-center w-5/6 mx-1 px-1 py-2 rounded"
                >
                  <p>{suggestion.description}</p>
                </div>
              )
            })}
          </div>
        </>
      )}
    </PlacesAutocomplete>
  )
}

export default MSearch
