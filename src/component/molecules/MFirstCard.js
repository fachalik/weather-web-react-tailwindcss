import React from "react"
import AButton from "../atom/AButton"

const MFirstCard = () => {
  return (
    <div className="container mx-auto flex justify-center">
      <div
        className="border border-gray bg-white w-5/6 mx-1 px-20 py-10 rounded flex flex-col justify-evenly"
        style={{ height: "70vh" }}
      >
        <p className="text-7xl font-montserrat whitespace-pre-line my-2">
          {`Get weather news  
          & compare it.`}
        </p>
        <p className="text-2xl my-2">
          Get the weather news and compare it between another city. We get
          weather data from <a href="https://www.weatherapi.com/" target="_blank" className="cursor-pointer text-cyan-700 font-semibold" rel="noreferrer">weather api</a>, and use <a href="https://reactjs.org/" target="_blank" className="cursor-pointer text-cyan-700 font-semibold" rel="noreferrer">React</a> with
          <a href="https://tailwindcss.com/" target="_blank" className="cursor-pointer text-cyan-700 font-semibold" rel="noreferrer"> tailwind css</a> for styling. Click button below to check the code
        </p>
        <div>
          <AButton
            icon="gitlab"
            className={"px-20 mr-1"}
            disabled={false}
            link={
              "https://gitlab.com/chalikfroza/weather-web-react-tailwindcss"
            }
            variant={"default"}
          >
            Gitlab
          </AButton>
          <AButton
            icon="web"
            className={"px-20"}
            disabled={false}
            link={""}
            variant={"outline"}
          >
            Portofolio
          </AButton>
        </div>
      </div>
    </div>
  )
}

export default MFirstCard
