import React from 'react'
import { Icon } from "@iconify/react"

const AButton = ({ icon, className, disabled, link, variant, children, onClick }) => {
  const addClassName = className ? `${className}` : ""

  const variants = {
    outline: `border border-cyan-900 text-cyan-900 rounded hover:bg-cyan-900 hover:text-white`,
    default: `bg-cyan-900 hover:bg-cyan-700 text-white rounded`,
    'rounded-outline': `border border-cyan-900 text-cyan-900 rounded-full hover:bg-cyan-900 hover:text-white`,
    'rounded-default': `bg-cyan-900 hover:bg-cyan-700 text-white rounded-full`,
    'default-disabled': `bg-slate-300 text-white cursor-not-allowed rounded`,
    'default-outline-disabled': `bg-slate-300 text-white cursor-not-allowed rounded-full`
  }

  const icons = {
    web: `mdi:web`,
    gitlab: `fa-brands:gitlab`
  }

  const pickedVariant = variants[variant]
  const pickedIcon = icons[icon]

  return (
    <>
      {
        !disabled ? (
          link === undefined ? (
            <button
              onClick={onClick ? onClick : null}
              className={`py-2 px-2 text-lg inline-block ${pickedVariant} ${addClassName}`}
            >
              <div className='flex justify-between items-center'>
                {
                  icon ? (
                    <Icon icon={pickedIcon} size={24} className='text-primary mr-2' />
                  ) : null
                }
                <p className='font-poppins font-medium'>
                  {children}
                </p>
              </div>
            </button>
          ) : (
            <a
              href={link}
              className={`py-2 px-2 text-lg inline-block ${pickedVariant} ${addClassName}`}
            >
              <div className='flex justify-between items-center'>
                {
                  icon ? (
                    <Icon icon={pickedIcon} size={24} className='text-primary mr-2' />
                  ) : null
                }
                <p className='font-poppins font-medium'>
                  {children}
                </p>
              </div>
            </a>
          )
        ) : (
          <button
            className={`py-2 px-2 text-lg inline-block ${pickedVariant} ${addClassName}`}
          >
            <div className='flex justify-between items-center'>
              {
                icon ? (
                  <Icon icon={pickedIcon} size={24} className='text-primary mr-2' />
                ) : null
              }
              <p className='font-poppins font-medium'>
                {children}
              </p>
            </div>
          </button>
        )
      }
    </>
  )
}

export default AButton
